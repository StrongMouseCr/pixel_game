﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form1 : Form
    {
        private Graphics graphics;
        private int resolution;
        private int rows;
        private int columns;
        private int[,] map;
        private string fileName;
        private string path;
        private int x1, y1, x2, y2;
        private bool isFirst;
        private Brush brush;
        private int intCell;

        public Form1()
        {
            InitializeComponent();
        }

        private void SaveB_Click(object sender, EventArgs e)
        {
            path = PathUser.Text + @"\" + nameFile.Text + ".txt";
            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.UTF8))
                {
                    for (int x = 0; x < columns; x++)
                    {
                        for (int y = 0; y < rows; y++)
                            sw.Write(map[x, y] + "@");
                        sw.Write("/");
                    }
                }
            }
            catch(Exception ex)
            {
                PathUser.Text = "!";
            }
            path = PathUser.Text;
        }

        private void LoadB_Click(object sender, EventArgs e)
        {
            path = PathUser.Text + @"\" + nameFile.Text + ".txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    ClearMap();
                    string text = sr.ReadToEnd();
                    string[] line = text.Split('/');
                    for (int x = 0; x < columns; x++)
                    {
                        string[] symbolInLine = line[x].Split('@');
                        for (int y = 0; y < rows; y++)
                            if (int.Parse(symbolInLine[y]) != 0)
                                map[x, y] = int.Parse(symbolInLine[y]);
                    }
                }
                DrawGrid();
                DrawMap();
            }
            catch (Exception ex)
            {
                nameFile.Text = "!";
            }
            path = PathUser.Text;
        }

        private void ParametersField()
        {
            resolution = (int)nudResolution.Value;
            rows = field.Height / resolution;
            columns = field.Width / resolution;
        }

        private bool ValidateMousePosition(int x, int y) => x >= 0 && y >= 0 && x < columns && y < rows;

        private void DrawSquare(int x, int y, Brush bruh)
        {
            graphics.FillRectangle(bruh, x * resolution, y * resolution, resolution, resolution);
            field.Refresh();
        }

        private void ClearCell(int x, int y)
        {
            graphics.FillRectangle(Brushes.Black, x * resolution, y * resolution, resolution, resolution);
            DrawGrid();
            DrawMap();
        }

        private void DrawSlide(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) // Закрашенная клетка
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                {
                    map[x, y] = intCell;
                    DrawSquare(x, y, brush);
                }
            }
            if (e.Button == MouseButtons.Right) // Очистить клетку
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                {
                    map[x, y] = 0;
                    ClearCell(x, y);
                }
            }
        }

        private void Draw(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) // Закрашенная клетка
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                {
                    map[x, y] = intCell;
                    DrawSquare(x, y, brush);
                }
            }
            if (e.Button == MouseButtons.Right) // Очистить клетку
            {
                var x = e.Location.X / resolution;
                var y = e.Location.Y / resolution;
                var validationMouse = ValidateMousePosition(x, y);
                if (validationMouse)
                {
                    map[x, y] = 0;
                    ClearCell(x, y);
                }
            }
            if (e.Button == MouseButtons.Middle)
            {
                if (isFirst)
                {
                    x1 = e.Location.X / resolution;
                    y1 = e.Location.Y / resolution;
                    var validationMouse = ValidateMousePosition(x1, y1);
                    if (validationMouse)
                    {
                        DrawSquare(x1, y1, Brushes.AliceBlue);
                        isFirst = false;
                    }
                }
                else
                {
                    x2 = e.Location.X / resolution;
                    y2 = e.Location.Y / resolution;
                    var validationMouse = ValidateMousePosition(x2, y2);
                    if (validationMouse)
                    {
                        DrawSquare(x2, y2, Brushes.AliceBlue);
                        isFirst = true;
                        fill();
                    }
                }
            }
        }

        private void DrawGrid()
        {
            ParametersField();
            field.Image = new Bitmap(field.Width, field.Height);
            graphics = Graphics.FromImage(field.Image);
            Pen pen = new Pen(Color.White);
            for (int x = 0; x <= columns; ++x)
                graphics.DrawLine(pen, x * resolution, 0, x * resolution, rows * resolution);
            for (int y = 0; y <= rows; ++y)
                graphics.DrawLine(pen, 0, y * resolution, columns * resolution, y * resolution);
        }

        private void DrawMap()
        {
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < rows; y++)
                {
                    if (map[x, y] == 1)
                        graphics.FillRectangle(Brushes.LightGreen, x * resolution, y * resolution, resolution, resolution);
                    else if (map[x, y] == 2)
                        graphics.FillRectangle(Brushes.Gray, x * resolution, y * resolution, resolution, resolution);
                    else if (map[x, y] == 3)
                        graphics.FillRectangle(Brushes.Aqua, x * resolution, y * resolution, resolution, resolution);
                    else if (map[x, y] == 4)
                        graphics.FillRectangle(Brushes.Red, x * resolution, y * resolution, resolution, resolution);
                    else if (map[x, y] == 5)
                        graphics.FillRectangle(Brushes.Chocolate, x * resolution, y * resolution, resolution, resolution);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ClearMap();
            DrawGrid();
            isFirst = true;
            brush = Brushes.LightGreen;
            intCell = 1;
            path = PathUser.Text;
        }

        private void ClearMap()
        {
            ParametersField();
            map = new int[columns, rows];
        }

        private void PersonB_Click(object sender, EventArgs e)
        {
            brush = Brushes.LightGreen;
            intCell = 1;
        }

        private void EarthB_Click(object sender, EventArgs e)
        {
            brush = Brushes.Gray;
            intCell = 2;
        }

        private void WaterB_Click(object sender, EventArgs e)
        {
            brush = Brushes.Aqua;
            intCell = 3;
        }

        private void EnemyB_Click(object sender, EventArgs e)
        {
            brush = Brushes.Red;
            intCell = 4;
        }

        private void WallB_Click(object sender, EventArgs e)
        {
            brush = Brushes.Chocolate;
            intCell = 5;
        }

        private void nothingB_Click(object sender, EventArgs e)
        {
            brush = Brushes.Black;
            intCell = 0;
        }

        private void nudResolution_ValueChanged(object sender, EventArgs e)
        {
            DrawGrid();
        }

        private void ExitB_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void fill()
        {
            if (x1 < x2)
            {
                if (y1 < y2)
                {
                    for (int x = x1; x < x2 + 1; x++)
                        for (int y = y1; y < y2 + 1; y++)
                            map[x, y] = intCell;
                }
                else
                {
                    for (int x = x1; x < x2 + 1; x++)
                        for (int y = y2; y < y1 + 1; y++)
                            map[x, y] = intCell;
                }
            }
            else
            {
                if (y1 < y2)
                {
                    for (int x = x2; x < x1 + 1; x++)
                        for (int y = y1; y < y2 + 1; y++)
                            map[x, y] = intCell;
                }
                else
                {
                    for (int x = x2; x < x1 + 1; x++)
                        for (int y = y2; y < y1 + 1; y++)
                            map[x, y] = intCell;
                }
            }
            DrawGrid();
            DrawMap();
        }
    }
}
