﻿namespace Game
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ExitB = new System.Windows.Forms.Button();
            this.panel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.nameFile = new System.Windows.Forms.TextBox();
            this.nudResolution = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadB = new System.Windows.Forms.Button();
            this.SaveB = new System.Windows.Forms.Button();
            this.field = new System.Windows.Forms.PictureBox();
            this.PersonB = new System.Windows.Forms.Button();
            this.EarthB = new System.Windows.Forms.Button();
            this.WaterB = new System.Windows.Forms.Button();
            this.EnemyB = new System.Windows.Forms.Button();
            this.WallB = new System.Windows.Forms.Button();
            this.nothingB = new System.Windows.Forms.Button();
            this.PathUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudResolution)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.field)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel);
            this.splitContainer1.Panel1.Controls.Add(this.nudResolution);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.LoadB);
            this.splitContainer1.Panel1.Controls.Add(this.SaveB);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.field);
            this.splitContainer1.Size = new System.Drawing.Size(1300, 860);
            this.splitContainer1.SplitterDistance = 236;
            this.splitContainer1.TabIndex = 0;
            // 
            // ExitB
            // 
            this.ExitB.AutoSize = true;
            this.ExitB.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ExitB.Location = new System.Drawing.Point(0, 680);
            this.ExitB.Name = "ExitB";
            this.ExitB.Padding = new System.Windows.Forms.Padding(10);
            this.ExitB.Size = new System.Drawing.Size(232, 47);
            this.ExitB.TabIndex = 8;
            this.ExitB.Text = "Выход";
            this.ExitB.UseVisualStyleBackColor = true;
            this.ExitB.Click += new System.EventHandler(this.ExitB_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Controls.Add(this.PathUser);
            this.panel.Controls.Add(this.label2);
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.nameFile);
            this.panel.Controls.Add(this.nothingB);
            this.panel.Controls.Add(this.ExitB);
            this.panel.Controls.Add(this.WallB);
            this.panel.Controls.Add(this.EnemyB);
            this.panel.Controls.Add(this.WaterB);
            this.panel.Controls.Add(this.EarthB);
            this.panel.Controls.Add(this.PersonB);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 90);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(234, 729);
            this.panel.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Location = new System.Drawing.Point(0, 641);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Название файла";
            // 
            // nameFile
            // 
            this.nameFile.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nameFile.Location = new System.Drawing.Point(0, 658);
            this.nameFile.Name = "nameFile";
            this.nameFile.Size = new System.Drawing.Size(232, 22);
            this.nameFile.TabIndex = 4;
            // 
            // nudResolution
            // 
            this.nudResolution.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nudResolution.Location = new System.Drawing.Point(0, 819);
            this.nudResolution.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.nudResolution.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudResolution.Name = "nudResolution";
            this.nudResolution.Size = new System.Drawing.Size(234, 22);
            this.nudResolution.TabIndex = 3;
            this.nudResolution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudResolution.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudResolution.ValueChanged += new System.EventHandler(this.nudResolution_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 841);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Разрешение";
            // 
            // LoadB
            // 
            this.LoadB.Dock = System.Windows.Forms.DockStyle.Top;
            this.LoadB.Location = new System.Drawing.Point(0, 44);
            this.LoadB.Name = "LoadB";
            this.LoadB.Size = new System.Drawing.Size(234, 46);
            this.LoadB.TabIndex = 1;
            this.LoadB.Text = "Загрузить";
            this.LoadB.UseVisualStyleBackColor = true;
            this.LoadB.Click += new System.EventHandler(this.LoadB_Click);
            // 
            // SaveB
            // 
            this.SaveB.Dock = System.Windows.Forms.DockStyle.Top;
            this.SaveB.Location = new System.Drawing.Point(0, 0);
            this.SaveB.Name = "SaveB";
            this.SaveB.Size = new System.Drawing.Size(234, 44);
            this.SaveB.TabIndex = 0;
            this.SaveB.Text = "Сохранить";
            this.SaveB.UseVisualStyleBackColor = true;
            this.SaveB.Click += new System.EventHandler(this.SaveB_Click);
            // 
            // field
            // 
            this.field.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.field.Dock = System.Windows.Forms.DockStyle.Fill;
            this.field.Location = new System.Drawing.Point(0, 0);
            this.field.Name = "field";
            this.field.Size = new System.Drawing.Size(1058, 858);
            this.field.TabIndex = 0;
            this.field.TabStop = false;
            this.field.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Draw);
            this.field.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DrawSlide);
            // 
            // PersonB
            // 
            this.PersonB.Dock = System.Windows.Forms.DockStyle.Top;
            this.PersonB.Location = new System.Drawing.Point(0, 0);
            this.PersonB.Name = "PersonB";
            this.PersonB.Size = new System.Drawing.Size(232, 46);
            this.PersonB.TabIndex = 9;
            this.PersonB.Text = "Выбрать Существо";
            this.PersonB.UseVisualStyleBackColor = true;
            this.PersonB.Click += new System.EventHandler(this.PersonB_Click);
            // 
            // EarthB
            // 
            this.EarthB.Dock = System.Windows.Forms.DockStyle.Top;
            this.EarthB.Location = new System.Drawing.Point(0, 46);
            this.EarthB.Name = "EarthB";
            this.EarthB.Size = new System.Drawing.Size(232, 46);
            this.EarthB.TabIndex = 11;
            this.EarthB.Text = "Выбрать Землю";
            this.EarthB.UseVisualStyleBackColor = true;
            this.EarthB.Click += new System.EventHandler(this.EarthB_Click);
            // 
            // WaterB
            // 
            this.WaterB.Dock = System.Windows.Forms.DockStyle.Top;
            this.WaterB.Location = new System.Drawing.Point(0, 92);
            this.WaterB.Name = "WaterB";
            this.WaterB.Size = new System.Drawing.Size(232, 46);
            this.WaterB.TabIndex = 13;
            this.WaterB.Text = "Выбрать Воду";
            this.WaterB.UseVisualStyleBackColor = true;
            this.WaterB.Click += new System.EventHandler(this.WaterB_Click);
            // 
            // EnemyB
            // 
            this.EnemyB.Dock = System.Windows.Forms.DockStyle.Top;
            this.EnemyB.Location = new System.Drawing.Point(0, 138);
            this.EnemyB.Name = "EnemyB";
            this.EnemyB.Size = new System.Drawing.Size(232, 46);
            this.EnemyB.TabIndex = 15;
            this.EnemyB.Text = "Выбрать Врага";
            this.EnemyB.UseVisualStyleBackColor = true;
            this.EnemyB.Click += new System.EventHandler(this.EnemyB_Click);
            // 
            // WallB
            // 
            this.WallB.Dock = System.Windows.Forms.DockStyle.Top;
            this.WallB.Location = new System.Drawing.Point(0, 184);
            this.WallB.Name = "WallB";
            this.WallB.Size = new System.Drawing.Size(232, 46);
            this.WallB.TabIndex = 17;
            this.WallB.Text = "Выбрать Cтену";
            this.WallB.UseVisualStyleBackColor = true;
            this.WallB.Click += new System.EventHandler(this.WallB_Click);
            // 
            // nothingB
            // 
            this.nothingB.Dock = System.Windows.Forms.DockStyle.Top;
            this.nothingB.Location = new System.Drawing.Point(0, 230);
            this.nothingB.Name = "nothingB";
            this.nothingB.Size = new System.Drawing.Size(232, 46);
            this.nothingB.TabIndex = 19;
            this.nothingB.Text = "Выбрать Ничего";
            this.nothingB.UseVisualStyleBackColor = true;
            this.nothingB.Click += new System.EventHandler(this.nothingB_Click);
            // 
            // PathUser
            // 
            this.PathUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PathUser.Location = new System.Drawing.Point(0, 293);
            this.PathUser.Name = "PathUser";
            this.PathUser.Size = new System.Drawing.Size(232, 22);
            this.PathUser.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Location = new System.Drawing.Point(0, 276);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Путь до файла";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 860);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.Text = "1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudResolution)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.field)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button LoadB;
        private System.Windows.Forms.Button SaveB;
        private System.Windows.Forms.PictureBox field;
        private System.Windows.Forms.NumericUpDown nudResolution;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameFile;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button ExitB;
        private System.Windows.Forms.Button EnemyB;
        private System.Windows.Forms.Button WaterB;
        private System.Windows.Forms.Button EarthB;
        private System.Windows.Forms.Button PersonB;
        private System.Windows.Forms.Button WallB;
        private System.Windows.Forms.Button nothingB;
        private System.Windows.Forms.TextBox PathUser;
        private System.Windows.Forms.Label label3;
    }
}

